package gamification.worker;

import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * @Class       : KeyGenerator
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Use to generate unique key
 *
 * @Comment(s)  : -
 */
public class KeyGenerator {
    private static KeyGenerator INSTANCE;

    /**
     * Singleton
     * @return unique KeyGenerator instance
     */
    public static KeyGenerator getInstance(){
        if(INSTANCE == null)
            INSTANCE = new KeyGenerator();

        return INSTANCE;
    }

    /**
     * Generate a key
     * @param keyLength Key's length
     * @return Key
     */
    public String generateKey(int keyLength) {
        //TODO check if "AES" constant exists
        javax.crypto.KeyGenerator keyGen;
        try {
            keyGen = javax.crypto.KeyGenerator.getInstance("AES");
            keyGen.init(keyLength);
            SecretKey secretKey = keyGen.generateKey();
            byte[] encoded = secretKey.getEncoded();
            return DatatypeConverter.printHexBinary(encoded).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get a random hexa string
     * @param numchars String size
     * @return Hexa string
     */
    public String getRandomHexString(int numchars){
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while(sb.length() < numchars){
            sb.append(Integer.toHexString(r.nextInt()));
        }

        return sb.toString().substring(0, numchars);
    }
}
