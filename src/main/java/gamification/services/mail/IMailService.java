package gamification.services.mail;

import javax.ejb.Local;

/**
 * @Class       : IMailService
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Interface to define local Mail Service DAO
 *
 * @Comment(s)  : -
 */
@Local
public interface IMailService {
    /**
     * Send email
     * @param subject Email's subject
     * @param content Email's content
     * @param receiptMail Email's destination
     * @throws Exception
     */
    void send(String subject, String content, String receiptMail) throws Exception;
}
