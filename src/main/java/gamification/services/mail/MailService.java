package gamification.services.mail;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;

/**
 * @Class       : MailService
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Implement Mail Service DAO Local
 *
 * @Comment(s)  : -
 * @See         : IMailService
 */
@Stateless
public class MailService implements IMailService {
    @Resource(lookup = "mail/MailSession")
    private Session session;

    @Override
    public void send(String subject, String content, String receiptMail) throws Exception {
        session.setDebug(true);
        Message msg = new MimeMessage(session);
        msg.setSubject(subject);
        msg.setContent(content,"text/html;charset=utf-8");
        msg.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiptMail));
        msg.setSentDate(new Date());
        Transport.send(msg);
    }
}
