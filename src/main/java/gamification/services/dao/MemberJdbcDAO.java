package gamification.services.dao;

import gamification.model.Application;
import gamification.model.Member;
import gamification.model.Role;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * @Class       : MemberJdbcDAO
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Implement Member DAO Local
 *
 * @Comment(s)  : -
 * @See         : MemberDAOLocal
 */
@Stateless
public class MemberJdbcDAO implements MemberDAOLocal {

    @Resource(lookup = "jdbc/AMTDatasource")
    private DataSource dataSource;

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAOLocal;

    @Override
    public boolean add(Member member, boolean mustThrowException) {
        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO members (name, email, password, enabled, role, password_resetted) value(?, ?, ?, ?, ?, ?)"
            );

            ps.setString(1, member.getName());
            ps.setString(2, member.getEmail());
            ps.setString(3, member.getPassword());
            ps.setBoolean(4, member.isEnabled());
            ps.setInt(5, member.getRole().ordinal());
            ps.setBoolean(6, false);

            // execute insert SQL statement
            ps.executeUpdate();

            ps.close();

            // Use only for no functional testing
            if(mustThrowException) {
                throw new RuntimeException();
            }
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public boolean add(Member member) {
        return memberDAOLocal.add(member, false);
    }

    @Override
    public boolean update(Member member) {
        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement(
                    "UPDATE members SET name=?, password=?, enabled=?, password_resetted=?, reset_token=?, role=? WHERE email=?"
            );

            ps.setString(1, member.getName());
            ps.setString(2, member.getPassword());
            ps.setBoolean(3, member.isEnabled());
            ps.setBoolean(4, member.isPasswordResetted());
            ps.setString(5, member.getResetToken());
            ps.setInt(6, member.getRole().ordinal());
            ps.setString(7, member.getEmail());

            // execute insert SQL statement
            ps.executeUpdate();

            ps.close();
            con.close();
        }catch (SQLException sqle) {
            sqle.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public int getNbUsers() {
        int result = 0;
        try {
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT count(*) as nbUsers FROM members");
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                result = rs.getInt("nbUsers");
            }

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Member> findAll(int offset, int number) {
        List<Member> members = new LinkedList<>();

        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM members LIMIT ?, ?");

            ps.setInt(1, offset);
            ps.setInt(2, number);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                String memberLogin = rs.getString("email");
                String memberPassword = rs.getString("password");
                String memberName = rs.getString("members.name");
                int memberRole = rs.getInt("role");
                boolean memberEnabled = rs.getBoolean("enabled");
                boolean memberPasswordResetted = rs.getBoolean("password_resetted");
                String memberToken = rs.getString("reset_token");
                boolean exist = false;

                    Member member = new Member(
                            memberName,
                            memberLogin,
                            memberPassword,
                            Role.values()[memberRole],
                            memberEnabled,
                            memberPasswordResetted,
                            memberToken);
                    members.add(member);

            }

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }

        return members;
    }


    @Override
    public Member findByToken(String token) {
        Member member = null;

        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM members WHERE reset_token = ? LIMIT 1");
            ps.setString(1, token);

            ResultSet rs = ps.executeQuery();

            member = getMemberByResultSet(rs);

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }

        return member;
    }

    @Override
    public Member findByLogin(String login) {
        Member member = null;

        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM members LEFT JOIN applications ON members.email = applications.fk_members WHERE email = ?");
            ps.setString(1, login);

            ResultSet rs = ps.executeQuery();

            member = getMemberByResultSet(rs);

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }

        return member;
    }

    private Member getMemberByResultSet(ResultSet rs) throws SQLException {
        Member member = null;
        if(rs.next()){
            member = new Member(
                    rs.getString("members.name"),
                    rs.getString("email"),
                    rs.getString("password"),
                    Role.values()[rs.getInt("role")],
                    rs.getBoolean("enabled"),
                    rs.getBoolean("password_resetted"),
                    rs.getString("reset_token"));

            while(rs.next()){
                String appName = rs.getString("applications.name");
                if(appName != null)
                    member.addApplication(new Application(appName, rs.getString("description"), member));
            }
        }

        return member;
    }

    @Override
    public boolean isMemberPresent(String login){
        return findByLogin(login) != null;
    }

    @Override
    public boolean isLoginCorrect(String login, String password){
        Member member = findByLogin(login);
        return member != null && member.isEnabled() && member.getPassword().equals(password);
    }

    @Override
    public boolean isAdmin(String login){
        return findByLogin(login).getRole() == Role.ADMINISTRATOR;
    }

    @Override
    public boolean remove(String email) {
        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement(
                    "DELETE FROM members WHERE email = ?"
            );

            ps.setString(1, email);

            // execute insert SQL statement
            ps.executeUpdate();

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
            return false;
        }

        return true;
    }
}