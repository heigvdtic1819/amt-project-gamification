package gamification.services.dao;

import gamification.model.Application;
import gamification.model.Member;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * @Class       : ApplicationJdbcDAO
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Implement Application DAO Local
 *
 * @Comment(s)  : -
 * @See         : ApplicationDAOLocal
 */
@Stateless
// Use only for no functional testing
// @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ApplicationJdbcDAO implements ApplicationDAOLocal {

    @Resource(lookup = "jdbc/AMTDatasource")
    private DataSource dataSource;

    @Override
    public boolean add(Application application) {
        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO applications (id, name, description, api_key, api_secret, fk_members) value(NULL, ?, ?, ?, ?, ?)"
            );

            ps.setString(1, application.getName());
            ps.setString(2, application.getDescription());
            ps.setString(3, application.getApiKey());
            ps.setString(4, application.getSecretKey());
            ps.setString(5, application.getMember().getEmail());

            // execute insert SQL statement
            ps.executeUpdate();

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public boolean remove(int applicationID) {
        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement(
                    "DELETE FROM applications WHERE ID = ?"
            );

            ps.setInt(1, applicationID);

            // execute insert SQL statement
            ps.executeUpdate();

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public int getNbAppByMember(Member member) {
        int result = 0;
        try {
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT count(*) as nbApp FROM applications INNER JOIN members ON members.email = applications.fk_members WHERE fk_members = ?");
            ps.setString(1, member.getEmail());
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                result = rs.getInt("nbApp");
            }

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }

        return result;

    }

    @Override
    public List<Application> findByMember(Member member, int offset, int number) {

        List<Application> apps = new LinkedList<>();

        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM applications INNER JOIN members ON members.email = applications.fk_members WHERE fk_members = ? LIMIT ?, ?");
            ps.setString(1, member.getEmail());
            ps.setInt(2, offset);
            ps.setInt(3, number);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                int id = rs.getInt("id");
                String appName = rs.getString("applications.name");
                boolean enabled = rs.getBoolean("applications.enabled");
                String appDescription = rs.getString("description");
                String apiKey = rs.getString("api_key");
                String apiSecret = rs.getString("api_secret");
                Application app = new Application(id,appName, appDescription, apiKey, apiSecret, member, enabled);
                apps.add(app);
            }

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }

        return apps;
    }

    @Override
    public Application findByMember(Member member, int appId) {

        Application app = null;

        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM applications INNER JOIN members ON members.email = applications.fk_members WHERE fk_members = ? AND applications.id = ?");
            ps.setString(1, member.getEmail());
            ps.setInt(2, appId);
            ResultSet rs = ps.executeQuery();

            if(rs.next()){
                int id = rs.getInt("id");
                boolean enabled = rs.getBoolean("applications.enabled");
                String appName = rs.getString("applications.name");
                String appDescription = rs.getString("description");
                String apiKey = rs.getString("api_key");
                String apiSecret = rs.getString("api_secret");
                app = new Application(id, appName, appDescription, apiKey, apiSecret, member, enabled);
            }

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }

        return app;
    }

    @Override
    public Application find(int appId) {
        Application app = null;

        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM applications WHERE applications.id = ?");
            ps.setInt(1, appId);
            ResultSet rs = ps.executeQuery();

            if(rs.next()){
                int id = rs.getInt("id");
                String appName = rs.getString("applications.name");
                boolean enabled = rs.getBoolean("applications.enabled");
                String appDescription = rs.getString("description");
                String apiKey = rs.getString("api_key");
                String apiSecret = rs.getString("api_secret");
                app = new Application(id, appName, appDescription, apiKey, apiSecret, null, enabled);
            }

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }

        return app;
    }

    @Override
    public boolean update(Application application) {
        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement(
                    "UPDATE applications SET name=?, description=?, enabled=? WHERE applications.id = ?"
            );

            ps.setString(1, application.getName());
            ps.setString(2, application.getDescription());
            ps.setBoolean(3, application.isEnabled());
            ps.setInt(4, application.getId());

            // execute insert SQL statement
            ps.executeUpdate();

            ps.close();
            con.close();
        }catch (SQLException sqle) {
            sqle.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public List<Application> findAll() {
        List<Application> apps = new LinkedList<>();

        try{
            Connection con = dataSource.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM applications");
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                String appName = rs.getString("applications.name");
                String appDescription = rs.getString("description");
                Application app = new Application(appName, appDescription, null);
                apps.add(app);
            }

            ps.close();
            con.close();

        }catch (SQLException sqle){
            sqle.printStackTrace();
        }

        return apps;
    }
}