package gamification.services.dao;

import gamification.model.Member;

import javax.ejb.Local;
import java.util.List;

/**
 * @Class       : MemberDAOLocal
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Interface to define local Member DAO
 *
 * @Comment(s)  : -
 */
@Local
public interface MemberDAOLocal {

    /**
     * Add member use only for testing
     * Add member
     * @param member The member to add
     * @param mustThrowException If the app must throw Exception
     * @return true -> success, false -> fail
     */
    boolean add(Member member, boolean mustThrowException);

    /**
     * Add member
     * @param member The member to add
     * @return true -> success, false -> fail
     */
    boolean add(Member member);

    /**
     * Get all members
     * @param offset Offset
     * @param number Limit
     * @return List of members
     */
    List<Member> findAll(int offset, int number);

    /**
     * Get the number of members
     * @return The number of members
     */
    int getNbUsers();

    /**
     * Find member by login
     * @param login The member's login
     * @return The member
     */
    Member findByLogin(String login);

    /**
     * Find member by token
     * @param token The member's token
     * @return The member
     */
    Member findByToken(String token);

    /**
     * Check if member is present in database
     * @param email The member's email
     * @return true -> present, false not present
     */
    boolean isMemberPresent(String email);

    /**
     * Check if member credentials are correct
     * @param email Member's email
     * @param password Member's password
     * @return true -> ok, false bad credentials
     */
    boolean isLoginCorrect(String email, String password);

    /**
     * Check if the member is an admin
     * @param login The member's login
     * @return true -> admin, false -> not admin
     */
    boolean isAdmin(String login);

    /**
     * Update the member properties
     * @param member The member to update
     * @return true -> success, false -> fail
     */
    boolean update(Member member);

    /**
     * Remove the member
     * @param email The member's email to remove
     * @return true -> success, false -> fail
     */
    boolean remove(String email);
}