package gamification.services.dao;

import gamification.model.Application;
import gamification.model.Member;

import javax.ejb.Local;
import java.util.List;

/**
 * @Class       : ApplicationDAOLocal
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Interface to define local Application DAO
 *
 * @Comment(s)  : -
 */
@Local
public interface ApplicationDAOLocal {
    /**
     * Add application
     * @param application The application to add
     * @return true -> success, false -> fail
     */
    boolean add(Application application);

    /**
     * Remove application
     * @param applicationID Application's id
     * @return true -> success, false -> fail
     */
    boolean remove(int applicationID);

    /**
     * Get an application from a member
     * @param member The member to get an application
     * @return true -> success, false -> fail
     */
    int getNbAppByMember(Member member);

    /**
     * Get all member's applications
     * @param member The member to get all applications
     * @param offset Offset
     * @param number Limit
     * @return The member's application list
     */
    List<Application> findByMember(Member member, int offset, int number);

    /**
     * Get all applications created
     * @return All applications created
     */
    List<Application> findAll();

    Application findByMember(Member member, int appId);
    Application find(int appId);
    boolean update(Application application);
}