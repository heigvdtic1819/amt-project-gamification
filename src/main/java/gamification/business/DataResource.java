package gamification.business;

/**
 * @Class       : DataResource
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : DataResource configurations
 *
 * @Comment(s)  : -
 */
public class DataResource {
    public static String PROTOCOL = "http://";
    public static String SERVER = "localhost";
    public static int SERVER_PORT = 8080;
    public static String BASE_URL = "/ch.heigvd.amt-1.0/";
    public static String VUE_ADMIN = "/WEB-INF/pages/api/admin.jsp";
    public static String VUE_APPLICATION = "/WEB-INF/pages/api/application.jsp";
    public static String VUE_APPLICATION_DETAIL = "/WEB-INF/pages/api/appdetail.jsp";
    public static String VUE_APPLICATION_LIST = "/WEB-INF/pages/api/applist.jsp";
    public static String VUE_LOGIN = "/WEB-INF/pages/login.jsp";
    public static String VUE_REGISTER = "/WEB-INF/pages/register.jsp";
    public static String VUE_FORGET = "/WEB-INF/pages/forget.jsp";
    public static String VUE_USER_DETAIL = "/WEB-INF/pages/api/member.jsp";
    public static String VUE_CHANGE_PASSWORD = "/WEB-INF/pages/changePassword.jsp";

    public static String ENDPOINT_APPLICATION_LIST = "applist";
    public static String ENDPOINT_LOGIN = BASE_URL + "login";
    public static String ENDPOINT_LOGOUT = BASE_URL + "logout";
    public static String ENDPOINT_APPLICATION = BASE_URL + "application";
    public static String ENDPOINT_ADMIN = BASE_URL + "admin";
    public static String ENDPOINT_CHANGE_PASSWORD = BASE_URL + "changePassword";
    public static String ENDPOINT_ADMIN_MEMBER = BASE_URL + "admin/member";
}
