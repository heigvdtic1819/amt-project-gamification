package gamification.tests;

import gamification.model.Application;
import gamification.model.Member;
import gamification.model.Role;
import gamification.services.dao.ApplicationDAOLocal;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.*;
import java.util.List;

/**
 * @Class       : ApplicationJdbcDAO
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Implement Application No Functionnal DAO Local
 *
 * @Comment(s)  : -
 * @See         : ApplicationDAOLocal
 */
@Stateless
// Use only for no functional testing
// @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ApplicationNoFunctionalTestDAO implements ApplicationNoFunctionalTestDAOLocal {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    @EJB(beanName = "ApplicationJdbcDAO")
    private ApplicationDAOLocal applicationDAO;

    @EJB(beanName = "ApplicationNoFunctionalTestDAO")
    private ApplicationNoFunctionalTestDAOLocal applicationNoFunctionalTestDAOLocal;

    private static Member exceptionMember;

    private static final int NUMBER_OF_APPLICATION_TO_CREATE = 4;

    private static Member memberWith100App;

    private static final int CREATE_1000_APP_NUMBER = 1000;

    private static int numberIdName;

    @Override
    public ApplicationNoFunctionalTestMemberInfo addApplicationNoFunctionalTestMemberInfo() {
        Member member = applicationNoFunctionalTestDAOLocal.addMember();
        //Member member = this.addMember();

        Member memberToInsert = member == null ? exceptionMember : member;

        for(int i = 0, j = numberIdName; i < NUMBER_OF_APPLICATION_TO_CREATE; ++i, ++j) {
            applicationDAO.add(
                    new Application("Application Name " + j, "Application Description " + j, memberToInsert)
            );
        }

        List<Application> applications =
                applicationDAO.findByMember(member, 0, applicationDAO.getNbAppByMember(member));

        return new ApplicationNoFunctionalTestMemberInfo(member, applications);
    }

    @Override
    public Member addMember() {
        Member member = createNormalMember();
        try {
            memberDAO.add(member, false);
        } catch (RuntimeException e) {
            return null;
        }

        return member;
    }

    @Override
    public void createExceptionMember() {
        exceptionMember = createMember("Exception Member");
        memberDAO.add(exceptionMember);
    }

    @Override
    public Member createNormalMember() {
        numberIdName = memberDAO.getNbUsers() + 1;
        String name = "Name" + numberIdName;
       return createMember(name);
    }

    public void createMemberWith1000App() {
        memberWith100App = createMember("1000AppMember");
        memberDAO.add(memberWith100App);

        for(int i = 0; i < CREATE_1000_APP_NUMBER; ++i) {
            applicationDAO.add(
                    new Application("Application Name " + i, "Application Description " + i, memberWith100App)
            );
        }
    }

    public ApplicationNoFunctionalTestMemberInfo getMemberWith1000App() {
        List<Application> applications =
                applicationDAO.findByMember(memberWith100App, 0, applicationDAO.getNbAppByMember(memberWith100App));

        return new ApplicationNoFunctionalTestMemberInfo(memberWith100App, applications);
    }

    private Member createMember(String name) {
        return new Member(
                name,
                name + "@test.ch",
                "password",
                Role.DEVELOPER,
                true,
                false,
                ""
        );
    }
}
