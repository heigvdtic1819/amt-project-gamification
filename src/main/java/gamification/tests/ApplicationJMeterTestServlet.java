package gamification.tests;

import gamification.model.Member;
import gamification.services.dao.ApplicationDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ApplicationJMeterTestServlet extends HttpServlet {

    @EJB(beanName = "ApplicationNoFunctionalTestDAO")
    ApplicationNoFunctionalTestDAOLocal applicationNoFunctionalTestDAOLocal;

    @EJB(beanName = "ApplicationJdbcDAO")
    private ApplicationDAOLocal applicationDAO;

    /**
     * Initialise the servlet and create an exception member
     * @param config The servlet config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        applicationNoFunctionalTestDAOLocal.createMemberWith1000App();
    }

    /**
     * Use to test transaction case
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            ApplicationNoFunctionalTestMemberInfo memberInfo = applicationNoFunctionalTestDAOLocal.getMemberWith1000App();

            resp.getWriter().println(memberInfo);
        } catch (Exception e) {
            resp.getWriter().println("There was a problem while create a member info");
        }
    }

}

