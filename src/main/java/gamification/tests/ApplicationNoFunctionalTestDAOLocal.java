package gamification.tests;

import gamification.model.Member;

import javax.ejb.Local;

/**
 * @Class       : ApplicationNoFunctionalTestDAO
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Only use to test no functional
 *
 * @Comment(s)  : -
 */
@Local
public interface ApplicationNoFunctionalTestDAOLocal {
    /**
     * Add a member with 4 applications
     * @return addApplicationNoFunctionalTestMemberInfo A model of addApplicationNoFunctionalTestMemberInfo
     */
    ApplicationNoFunctionalTestMemberInfo addApplicationNoFunctionalTestMemberInfo();

    /**
     * Add a member
     */
    Member addMember();

    /**
     * Create a member to use it if there is exception and create applications
     */
    void createExceptionMember();

    /**
     * Create a normal member
     * @return A normal member
     */
    Member createNormalMember();

    /**
     * Create member with 1000 app
     */
    void createMemberWith1000App();

    /**
     * Get a member with 1000 app
     * @return A ApplicationNoFunctionalTestMemberInfo
     */
    ApplicationNoFunctionalTestMemberInfo getMemberWith1000App();
}
