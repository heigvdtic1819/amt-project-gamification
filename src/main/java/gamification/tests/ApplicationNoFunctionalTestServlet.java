package gamification.tests;

import gamification.services.dao.ApplicationDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Class       : ApplicationNoFunctionalTestServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Use to test no application's no functional tests
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class ApplicationNoFunctionalTestServlet extends HttpServlet {

    @EJB(beanName = "ApplicationNoFunctionalTestDAO")
    ApplicationNoFunctionalTestDAOLocal applicationNoFunctionalTestDAOLocal;

    @EJB(beanName = "ApplicationJdbcDAO")
    private ApplicationDAOLocal applicationDAO;

    /**
     * Initialise the servlet and create an exception member
     * @param config The servlet config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        applicationNoFunctionalTestDAOLocal.createExceptionMember();
    }

    /**
     * Use to test transaction case
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long numberOfApplicationsBefore = -1;
        long numberOfApplicationsAfter;

        try {
            numberOfApplicationsBefore = applicationDAO.findAll().size();

            ApplicationNoFunctionalTestMemberInfo memberInfo = applicationNoFunctionalTestDAOLocal.addApplicationNoFunctionalTestMemberInfo();

            resp.getWriter().println(memberInfo);
        } catch (Exception e) {
            resp.getWriter().println("There was a problem while create a member info");
        } finally {
            numberOfApplicationsAfter = applicationDAO.findAll().size();
            resp.getWriter().println(String.format("Number of applications before: %d", numberOfApplicationsBefore));
            resp.getWriter().println(String.format("Number of applications after: %d", numberOfApplicationsAfter));
        }
    }

}
