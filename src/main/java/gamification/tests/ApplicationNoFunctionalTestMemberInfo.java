package gamification.tests;

import gamification.model.Application;
import gamification.model.Member;

import java.util.List;

/**
 * @Class       : Member
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Member Info Application N oFunctionnal Test model
 *
 * @Comment(s)  : -
 */
public class ApplicationNoFunctionalTestMemberInfo {
    private Member member;
    private List<Application> applications;

    public ApplicationNoFunctionalTestMemberInfo(Member member, List<Application> applications) {
        this.member = member;
        this.applications = applications;
    }

    /**
     * Get the member
     * @return The member
     */
    public Member getMember() {
        return member;
    }

    /**
     * Get the member's applications
     * @return The member's applications
     */
    public List<Application> getApplications() {
        return applications;
    }

    /**
     * Display the member info
     * @return String of the member info
     */
    @Override
    public String toString() {
        String memberInfo =
         "MemberInfo {" +
                "member=" + member.getName() + " " + member.getEmail() +
                ", applications= [";

        for(int i = 0; i < applications.size(); ++i) {
            memberInfo += applications.get(i).getName() + " " + applications.get(i).getDescription();
            memberInfo += i + 1 == applications.size() ? "" : ", ";
        }

        memberInfo +=  "]} ";

        return memberInfo;
    }
}
