package gamification.presentation.filter;

import gamification.business.DataResource;
import gamification.model.Application;
import gamification.model.Member;
import gamification.services.dao.ApplicationDAOLocal;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Class       : AppDetailFilter
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Moska & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Filter access to app detail
 *
 * @Comment(s)  : -
 * @See         : Filter
 */
public class AppDetailFilter implements Filter {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    @EJB(beanName = "ApplicationJdbcDAO")
    private ApplicationDAOLocal applicationDAO;

    private ServletContext context;

    /**
     * Init the Filter
     * @param fConfig The filter's context
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        this.context.log("Admin filter initialized");
    }

    /**
     * Apply Filter
     * @param request The request
     * @param response The respond
     * @param chain The next filter if we want to chain the filters
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession session = req.getSession(false);
        String user = null;
        if(session != null)
            user = (String)session.getAttribute("user");

        int appId = Integer.valueOf(req.getParameter("id"));
        Member currentUser = memberDAO.findByLogin(user);
        Application app = applicationDAO.findByMember(currentUser, appId);

        if(app == null){
            boolean isAdmin = memberDAO.isAdmin(user);
            if(isAdmin)
                app = applicationDAO.find(appId);
        }

        if(app != null){
            req.setAttribute("app", app);
            chain.doFilter(request, response);
        }else{
            res.sendRedirect(DataResource.ENDPOINT_APPLICATION_LIST);
        }
    }

    @Override
    public void destroy() {

    }

}
