package gamification.presentation.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @Class       : BaseFilter
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Define the base url if the next filter want to redirect
 *
 * @Comment(s)  : -
 * @See         : Filter
 */
public class BaseFilter implements Filter {

    private ServletContext context;

    /**
     * Init the Filter
     * @param fConfig The filter's context
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        this.context.log("Base filter initialized");
    }

    /**
     * Apply Filter
     * @param request The request
     * @param response The respond
     * @param chain The next filter if we want to chain the filters
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        String requestURL = req.getRequestURL().toString();
        String servletPath = req.getServletPath();
        String pathInfos = req.getPathInfo();
        requestURL = requestURL.replace(servletPath, "");
        if(pathInfos != null)
            requestURL = requestURL.replace(pathInfos, "");

        req.setAttribute("baseUrl", requestURL);
        chain.doFilter(request, response);
    }

    /**
     * Close resources
     */
    @Override
    public void destroy() {

    }

}
