package gamification.presentation.filter;

import gamification.business.DataResource;
import gamification.model.Member;
import gamification.services.dao.MemberDAOLocal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Class       : AuthenticationFilter
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Force the user to login if it is not the case
 *
 * @Comment(s)  : -
 * @See         : Filter
 */
public class AuthenticationFilter implements Filter {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    private ServletContext context;

    private List<String> nonAuthRoutes;

    /**
     * Init the Filter
     * @param fConfig The filter's context
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        this.nonAuthRoutes = new ArrayList<>();
        this.nonAuthRoutes.add("/login");
        this.nonAuthRoutes.add("/register");
        this.nonAuthRoutes.add("/retrieve");
        this.nonAuthRoutes.add("/forget");
        this.nonAuthRoutes.add("/tests");
        this.nonAuthRoutes.add("/1000App");

        this.context.log("Authentication filter initialized");
    }

    /**
     * Apply Filter
     * @param request The request
     * @param response The respond
     * @param chain The next filter if we want to chain the filters
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String servletPath = req.getServletPath();

        HttpSession session = req.getSession(false);
        String user = null;
        Member member = null;

        //Check if user is already login
        if(session != null)
            user = (String)session.getAttribute("user");

        //If not login check token to log in
        if(user == null){
            String token = request.getParameter("token");
            member = memberDAO.findByToken(token);
        }else{
            member = memberDAO.findByLogin(user);
        }
        //Set the member in the request
        req.setAttribute("user", member);

        if(member == null){
            if(!nonAuthRoutes.contains(servletPath)){
                this.context.log("Unauthorized access request");
                res.sendRedirect(DataResource.ENDPOINT_LOGIN);
                return;
            }
        }else{
            boolean isResetted = member.isPasswordResetted();
            boolean isForgotten = member.getResetToken() != null && !member.getResetToken().isEmpty();
            if((isForgotten || isResetted) && !servletPath.equals("/changePassword")){
                res.sendRedirect(DataResource.ENDPOINT_CHANGE_PASSWORD);
                return;
            }
            if(nonAuthRoutes.contains(servletPath)){
                res.sendRedirect(DataResource.ENDPOINT_APPLICATION);
                return;
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * Close resources
     */
    @Override
    public void destroy() {
        //close any resources here
    }

}
