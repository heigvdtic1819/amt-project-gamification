package gamification.presentation.filter;

import gamification.business.DataResource;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Class       : AdminFilter
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Define the current user rights
 *
 * @Comment(s)  : -
 * @See         : Filter
 */
public class AdminFilter implements Filter {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    private ServletContext context;

    /**
     * Init the Filter
     * @param fConfig The filter's context
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        this.context.log("Admin filter initialized");
    }

    /**
     * Apply Filter
     * @param request The request
     * @param response The respond
     * @param chain The next filter if we want to chain the filters
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession session = req.getSession(false);
        String user = null;
        if(session != null)
            user = (String)session.getAttribute("user");

        if(memberDAO.isAdmin(user)){
            chain.doFilter(request, response);
        }else{
            res.sendRedirect(DataResource.ENDPOINT_APPLICATION);
        }
    }

    /**
     * Close resources
     */
    @Override
    public void destroy() {
        //close any resources here
    }

}
