package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.model.Application;
import gamification.model.Member;
import gamification.services.dao.ApplicationDAOLocal;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Class       : ApplicationDetailServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Moska & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage the form to update/delete an application
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class ApplicationDetailServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    @EJB(beanName = "ApplicationJdbcDAO")
    private ApplicationDAOLocal applicationDAO;

    private static final String CHAMP_NOM = "nom";
    private static final String CHAMP_DESCRIPTION = "description";
    private static final String ATT_ERREURS = "erreurs";
    private static final String ATT_RESULTAT = "resultat";
    private static final String ATT_CHAMPS = "champs";

    /**
     * Remove member's application
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Application app = (Application) req.getAttribute("app");
        applicationDAO.remove(app.getId());
        resp.setStatus(200);
    }

    /**
     * Redirect user to complete a form to add new application
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(DataResource.VUE_APPLICATION_DETAIL).forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Application app = (Application) req.getAttribute("app");
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        boolean enabled = req.getParameter("enabled") != null && req.getParameter("enabled").equals("on");
        app.setName(name);
        app.setDescription(description);
        app.setEnabled(enabled);
        if(applicationDAO.update(app)){
            req.setAttribute("successMessage", "Update ok");
        }else{
            req.setAttribute("errorMessage", "Error");
        }
        this.getServletContext().getRequestDispatcher(DataResource.VUE_APPLICATION_DETAIL).forward(req,resp);
    }
}
