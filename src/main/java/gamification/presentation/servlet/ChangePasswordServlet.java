package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.model.Member;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Class       : AppListServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage the administration to reset member's password
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class ChangePasswordServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    /**
     * If request with get redirect to the form to reset password page
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(DataResource.VUE_CHANGE_PASSWORD).forward(req,resp);
    }

    /**
     * Check the form submited by member and update password if it is ok, else display an error
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newPassword = req.getParameter("password");
        String repeatNewPassword = req.getParameter("repeat");
        if(newPassword == null || !newPassword.equals(repeatNewPassword)){
            sendError("Password's must be equals", req, resp);
            return;
        }
        Member user = (Member) req.getAttribute("user");//Give by the filter we know it is not null
        user.setResetToken("");
        user.setPasswordResetted(false);
        user.setPassword(newPassword);
        if(memberDAO.update(user)){
            //TO TEST
            //Deliberate logout user after each change password
            resp.sendRedirect(DataResource.ENDPOINT_LOGOUT);
        }else{
            sendError("An error has occured", req, resp);
        }
    }

    /**
     * Send error to the form to display it to member
     * @param error The error to display
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    private void sendError(String error, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        req.setAttribute("errorMessage", error);
        req.getRequestDispatcher(DataResource.VUE_CHANGE_PASSWORD).forward(req,resp);
    }
}
