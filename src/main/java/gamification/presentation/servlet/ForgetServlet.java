package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.model.Member;
import gamification.services.mail.IMailService;
import gamification.services.dao.MemberDAOLocal;
import gamification.worker.KeyGenerator;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Class       : ForgetServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage the case if user want to receive an email to reset password
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class ForgetServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    @EJB(beanName = "MailService")
    private IMailService mailService;

    /**
     * If request with get redirect to the form to reset password page
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(DataResource.VUE_FORGET).forward(req,resp);
    }

    /**
     * Check the form submited by member and send email if it is ok, else display an error
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        if (email == null || !email.contains("@")) {
            req.setAttribute("errorMessage", "Invalid email");
        }else{
            //Check email existence
            Member member = memberDAO.findByLogin(email);
            if(member == null){
                req.setAttribute("errorMessage", "This email does not exist");
            }else{
                //Send reset message
                member.setResetToken(KeyGenerator.getInstance().generateKey(128));
                memberDAO.update(member);
                if(sendResetMail(email, member.getResetToken())){
                    req.setAttribute("successMessage", "An email to reset your password as been sent to your inbox");
                }else{
                    req.setAttribute("errorMessage", "An error has occured");
                }
            }
        }

        req.getRequestDispatcher(DataResource.VUE_FORGET).forward(req, resp);
    }

    /**
     * Send an email to member to reset password
     * @param email The member's email
     * @param retreive_token The tocken to reset password
     * @return true -> email was sent, false -> error when try to sent email
     */
    private boolean sendResetMail(String email, String retreive_token){
        String baseUrl = DataResource.PROTOCOL + DataResource.SERVER + ":" + DataResource.SERVER_PORT + DataResource.BASE_URL;
        baseUrl += "changePassword?token=" + retreive_token;
        String title = "Forget password";
        String body = "Please go to this link to change your password: \n";
        body += baseUrl;
        try {
            mailService.send(title, body, email);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
