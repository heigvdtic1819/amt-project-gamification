package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.model.Member;
import gamification.services.mail.IMailService;
import gamification.services.dao.MemberDAOLocal;
import gamification.worker.KeyGenerator;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * @Class       : ResetServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage when the user has resetted password
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class ResetServlet extends HttpServlet {

    @EJB(beanName = "MailService")
    private IMailService mailService;

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    /**
     * Send email to user to indicate that is password was resetted or display a message to user if it did not work
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        Member member = memberDAO.findByLogin(email);
        member.setPasswordResetted(true);
        member.setPassword(KeyGenerator.getInstance().getRandomHexString(6));
        req.setAttribute("member", member);
        if(memberDAO.update(member)){
            try {
                mailService.send("Your password has been resetted...",
                        "Please login with this new password and change it immediatly!\n" +
                                member.getPassword(),
                        email);
                req.setAttribute("successMessage", "Reset ok");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            req.setAttribute("errorMessage", "Unexpected error");
        }
        resp.sendRedirect(DataResource.ENDPOINT_ADMIN_MEMBER + "?email=" + email);
        //req.getRequestDispatcher(DataResource.VUE_USER_DETAIL).forward(req, resp);
    }
}
