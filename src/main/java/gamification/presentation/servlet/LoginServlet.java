package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * @Class       : LoginServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage the login form
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class LoginServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    /**
     * If request with get redirect to the form to display the form
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("register") != null && req.getParameter("register").equals("ok")) {
            req.setAttribute("successMessage", "Registered successfully.");
        }
        req.getRequestDispatcher(DataResource.VUE_LOGIN).forward(req, resp);
    }

    /**
     * Check the form submited by member and log the user if it is ok, else display an error
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        if(login == null || !login.contains("@")) {
            sendError("Invalid login", req, resp);
            return;
        }
        if(password == null || password.isEmpty()) {
            sendError("Password can't be empty", req, resp);
            return;
        }


        if(!memberDAO.isLoginCorrect(login, password)){
            sendError("Login/password not match or account suspended", req, resp);
            return;
        }

        HttpSession session = req.getSession();
        session.setAttribute("user", login);
        //setting session to expiry in 30 mins
        session.setMaxInactiveInterval(30*60);
        Cookie userName = new Cookie("user", login);
        userName.setMaxAge(30*60);
        resp.addCookie(userName);

        if(memberDAO.isAdmin(login)){
            resp.sendRedirect(DataResource.ENDPOINT_ADMIN);
        }else{
            resp.sendRedirect(DataResource.ENDPOINT_APPLICATION);
        }

    }

    /**
     * Send error to the form to display it to member
     * @param error The error to display
     * @param request The request
     * @param response The respond
     * @throws ServletException
     * @throws IOException
     */
    private void sendError(String error, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        request.setAttribute("errorMessage", error);
        request.getRequestDispatcher(DataResource.VUE_LOGIN).forward(request, response);
    }


}
