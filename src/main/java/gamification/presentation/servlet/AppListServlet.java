package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.model.Application;
import gamification.model.Member;
import gamification.services.dao.ApplicationDAOLocal;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Class       : AppListServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage the administration of members' account pagination for the users
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class AppListServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    @EJB(beanName = "ApplicationJdbcDAO")
    private ApplicationDAOLocal applicationDAO;


    /**
     * Manage the administration of members' account pagination for the users
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int nbElementsPerPage = 0;
        try {
            nbElementsPerPage = Integer.parseInt(req.getParameter("nbElementPerPage"));
        } catch (NumberFormatException e) {
            nbElementsPerPage = 5;
        }

        int pageIndex;
        try {
            pageIndex = Integer.parseInt(req.getParameter("pageIndex"));
        } catch (NumberFormatException e) {
            pageIndex = 0;
        }
        String userLogin = (String)req.getSession(false).getAttribute("user");

        Member currentUser = memberDAO.findByLogin(userLogin);
        int numberOfPages = (int)Math.ceil(applicationDAO.getNbAppByMember(currentUser) / (double)nbElementsPerPage);
        if(pageIndex > numberOfPages - 1 )
            pageIndex = numberOfPages - 1;
        List<Application> applications = applicationDAO.findByMember(currentUser, pageIndex * nbElementsPerPage, nbElementsPerPage);


        req.setAttribute("numberOfPages", numberOfPages);
        req.setAttribute("pageIndex", pageIndex);
        req.setAttribute("nbElementsPerPage", nbElementsPerPage);

        req.setAttribute("applications", applications);

        req.getRequestDispatcher(DataResource.VUE_APPLICATION_LIST).forward(req, resp);
    }

    /**
     * If request with post redirect to the application list page
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(DataResource.VUE_APPLICATION_LIST).forward(req, resp);
    }
}
