package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.model.Application;
import gamification.model.Member;
import gamification.services.dao.ApplicationDAOLocal;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Class       : ApplicationServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage the form to register an application
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class ApplicationServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    @EJB(beanName = "ApplicationJdbcDAO")
    private ApplicationDAOLocal applicationDAO;

    private static final String CHAMP_NOM = "nom";
    private static final String CHAMP_DESCRIPTION = "description";
    private static final String ATT_ERREURS = "erreurs";
    private static final String ATT_RESULTAT = "resultat";
    private static final String ATT_CHAMPS = "champs";
    private static final String ATT_APPLI = "applications";


    /**
     * Redirect user to complete a form to add new application
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(DataResource.VUE_APPLICATION).forward(req,resp);
    }

    /**
     * Check the form submited by member and add the applications if ok, else display errors
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String resultat = "Echec lors de l'enregistrement de l'application";
        Map<String, String> erreurs = new HashMap<String, String>();
        Map<String, String> champs = new HashMap<String, String>();


        String userLogin = (String)req.getSession(false).getAttribute("user");
        Member currentUser = memberDAO.findByLogin(userLogin);

        String nom = req.getParameter(CHAMP_NOM);
        String description = req.getParameter(CHAMP_DESCRIPTION);

        try{
            validationNom(nom);
            champs.put(CHAMP_NOM, nom);
        }catch (Exception e){
            erreurs.put(CHAMP_NOM, e.getMessage());
        }

        //TODO check why description text is not saved
        try{
            validationDescription(description);
            champs.put(CHAMP_DESCRIPTION, description);
        }catch (Exception e){
            erreurs.put(CHAMP_DESCRIPTION, e.getMessage());
        }

        req.setAttribute(ATT_ERREURS, erreurs);
        req.setAttribute(ATT_RESULTAT, resultat);

        if(erreurs.isEmpty()){
            resultat = "Application enregistrée avec succès";
            req.setAttribute(ATT_RESULTAT, resultat);
            req.setAttribute(ATT_CHAMPS, champs);
            applicationDAO.add(new Application(nom, description, currentUser));
            //redirect vers app list
            resp.sendRedirect(DataResource.ENDPOINT_APPLICATION_LIST);
        }else{
            this.getServletContext().getRequestDispatcher(DataResource.VUE_APPLICATION).forward(req,resp);
        }
    }

    /**
     * Check if the application's name is valid
     * @param name The name to check
     * @throws Exception
     */
    private void validationNom (String name) throws Exception{
        if(name != null && name.trim().length() < 3){
            throw new Exception("Le nom de l'application doit contenir au moins 3 caractères.");
        }
    }

    /**
     * Check if the application's description is valid
     * @param description The name to check
     * @throws Exception
     */
    private void validationDescription (String description) throws Exception{
        if(description != null && description.trim().length() < 3){
            throw new Exception("La description de l'application doit contenir au moins 10 caractères.");
        }
    }
}
