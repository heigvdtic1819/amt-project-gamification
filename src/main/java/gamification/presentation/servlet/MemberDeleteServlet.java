package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.services.dao.ApplicationDAOLocal;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Class       : MemberDeleteServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Delete a member account
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class MemberDeleteServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    /**
     * Delete a member account if exist
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("user");
        if(username != null){
            //remove user
            memberDAO.remove(username);
        }

        resp.sendRedirect(DataResource.ENDPOINT_LOGIN);
    }
}
