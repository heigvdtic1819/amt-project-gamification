package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.model.Application;
import gamification.model.Member;
import gamification.model.Role;
import gamification.services.dao.ApplicationDAOLocal;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Class       : MemberServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage the route when user is logged
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class MemberServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    @EJB(beanName = "ApplicationJdbcDAO")
    private ApplicationDAOLocal appDAO;

    /**
     * Manage the route when user is logged
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String currentEmail = req.getParameter("email");
        Member member = null;
        if(currentEmail != null && (member = memberDAO.findByLogin(currentEmail)) != null) {

            String name = req.getParameter("name");
            if(name.length() > 0) {
                member.setName(name);
            }
            int role = Integer.parseInt(req.getParameter("role"));
            if (role >= 0) {
                member.setRole(Role.values()[role]);
            }
            boolean enabled = req.getParameter("enabled") != null && req.getParameter("enabled").equals("on");
            member.setEnabled(enabled);
            memberDAO.update(member);
            resp.sendRedirect(DataResource.ENDPOINT_ADMIN);
        } else {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    /**
     * Manage the route when user is logged
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String currentEmail = req.getParameter("email");
        Member member = null;
        if(currentEmail != null && (member = memberDAO.findByLogin(currentEmail)) != null) {
            req.setAttribute("member", member);
            int nbElementsPerPage = 0;
            try {
                nbElementsPerPage = Integer.parseInt(req.getParameter("nbElementPerPage"));
            } catch (NumberFormatException e) {
                nbElementsPerPage = 5;
            }

            int pageIndex;
            try {
                pageIndex = Integer.parseInt(req.getParameter("pageIndex"));
            } catch (NumberFormatException e) {
                pageIndex = 0;
            }
            int numberOfPages = (int)Math.ceil(appDAO.getNbAppByMember(member) / (double)nbElementsPerPage);
            if(pageIndex > numberOfPages - 1 )
                pageIndex = numberOfPages - 1;
            List<Application> applications = appDAO.findByMember(member, pageIndex * nbElementsPerPage, nbElementsPerPage);

            req.setAttribute("numberOfPages", numberOfPages);
            req.setAttribute("pageIndex", pageIndex);
            req.setAttribute("nbElementsPerPage", nbElementsPerPage);

            req.setAttribute("applications", applications);

            req.getRequestDispatcher(DataResource.VUE_USER_DETAIL).forward(req, resp);
        } else {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    /**
     * Delete a member
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String currentEmail = req.getParameter("email");
        this.memberDAO.remove(currentEmail);
        resp.setStatus(200);
    }
}
