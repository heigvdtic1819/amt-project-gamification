package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.model.Member;
import gamification.services.dao.MemberDAOLocal;
import gamification.model.Role;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Class       : RegisterServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage member registration
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class RegisterServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    /**
     * If request with get redirect to the form to register page
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(DataResource.VUE_REGISTER).forward(req,resp);
    }

    /**
     * Check the form submited by member and log the member if it is ok, else display an error
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String repeat = req.getParameter("repeat");

        //Check login email
        if(login == null || !login.contains("@")){
            sendError("Invalid login", req, resp);
            return;
        }

        if(name == null || name.isEmpty()){
            sendError("You must provide a username", req, resp);
            return;
        }

        //Check if user already present
        if(memberDAO.isMemberPresent(login)){
            sendError("User already created", req, resp);
            return;
        }

        //Check password similitude
        if(repeat == null || repeat.isEmpty() || !repeat.equals(password)) {
            sendError("Password's must be the same and not empty", req, resp);
            return;
        }

        //Add user to list
        boolean result = memberDAO.add(new Member(name, login, password, Role.DEVELOPER, true, false, ""));

        //Send redirect to login page
        if(result){
            resp.sendRedirect(DataResource.ENDPOINT_LOGIN + "?register=ok");
        }else{
            req.setAttribute("errorMessage", "Error occured");
            req.getRequestDispatcher(DataResource.VUE_REGISTER).forward(req, resp);
        }
    }

    /**
     * Send error to the form to display it to member
     * @param error The error to display
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    private void sendError(String error, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        req.setAttribute("errorMessage", error);
        req.getRequestDispatcher(DataResource.VUE_REGISTER).forward(req, resp);
    }
}
