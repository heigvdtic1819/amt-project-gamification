package gamification.presentation.servlet;

import gamification.business.DataResource;
import gamification.model.Member;
import gamification.services.dao.MemberDAOLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Class       : AdminServlet
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Manage the administration of members' account pagination for the administrators
 *
 * @Comment(s)  : -
 * @See         : HttpServlet
 */
public class AdminServlet extends HttpServlet {

    @EJB(beanName = "MemberJdbcDAO")
    private MemberDAOLocal memberDAO;

    /**
     * Manage the administration of members' account pagination for the administrators
     * @param req The request
     * @param resp The respond
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int nbElementsPerPage = 0;
        try {
            nbElementsPerPage = Integer.parseInt(req.getParameter("nbElementPerPage"));
        } catch (NumberFormatException e) {
            nbElementsPerPage = 5;
        }
        int pageIndex = 0;
        try {
            pageIndex = Integer.parseInt(req.getParameter("pageIndex"));
        } catch (NumberFormatException e) {
            //pageIndex = 0;
        }
        int numberOfPages = (int)Math.ceil(memberDAO.getNbUsers() / (double)nbElementsPerPage);
        if(pageIndex > numberOfPages - 1 )
            pageIndex = numberOfPages - 1;

        List<Member> members = this.memberDAO.findAll(pageIndex * nbElementsPerPage, nbElementsPerPage);
        req.setAttribute("numberOfPages", numberOfPages);
        req.setAttribute("pageIndex", pageIndex);
        req.setAttribute("nbElementsPerPage", nbElementsPerPage);
        req.setAttribute("members", members);
        req.getRequestDispatcher(DataResource.VUE_ADMIN).forward(req, resp);
    }
}
