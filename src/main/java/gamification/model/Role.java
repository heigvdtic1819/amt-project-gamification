package gamification.model;

/**
 * @Class       : Role
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Define the user application roles
 *
 * @Comment(s)  : -
 */
public enum Role {
    ADMINISTRATOR, DEVELOPER
}
