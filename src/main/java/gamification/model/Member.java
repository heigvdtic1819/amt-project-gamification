package gamification.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @Class       : Member
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Member model
 *
 * @Comment(s)  : -
 */
public class Member {
    private String name;
    private String email;
    private String password;
    private Role role;
    private boolean enabled;
    private boolean isPasswordResetted;
    private String resetToken;
    private List<Application> applications;

    public Member(String name, String email, String password, Role role, boolean enabled, boolean isPasswordResetted, String resetToken) {
        this(name, email, password, role, enabled, isPasswordResetted, resetToken, new ArrayList<>());
    }

    public Member(String name, String email, String password, Role role, boolean enabled, boolean isPasswordResetted, String resetToken, List<Application> applications) {
        this.name = name;
        this.email = email;
        this.setPassword(password);//To hash password
        this.role = role;
        this.enabled = enabled;
        this.isPasswordResetted = isPasswordResetted;
        this.resetToken = resetToken;
        this.applications = applications;
    }

    /**
     * Get the member's email
     * @return The member's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the member's email
     * @param email The member's email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the member's password
     * @return The member's password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the member's password
     * @param password The member's password
     */
    //TODO HASH PASSWORD
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get the member's role
     * @return The member's role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Set the member's role
     * @param role The member's role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Define if the member's account is enabled
     * @return true -> enabled, false disabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Set the member's account state
     * @param enabled The new state
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Define if the member's password was resetted
     * @return true -> resetted, false -> not resetted
     */
    public boolean isPasswordResetted() {
        return isPasswordResetted;
    }

    /**
     * Set the password reset state
     * @param passwordResetted The new password reset state
     */
    public void setPasswordResetted(boolean passwordResetted) {
        this.isPasswordResetted = passwordResetted;
    }

    /**
     * Get the member's name
     * @return The member's name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the member's name
     * @param name The member's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Add application to member's application list
     * @param application The application to add
     */
    public void addApplication(Application application){
        this.applications.add(application);
    }

    /**
     * Remove application from member's application list
     * @param application The application to remove
     */
    public void removeApplication(Application application){
        this.applications.remove(application);
    }

    /**
     * Get the member's application list
     * @return The member's application list
     */
    public List<Application> getApplications() {
        return applications;
    }

    /**
     * Get the reset token
     * @return The reset token
     */
    public String getResetToken() {
        return resetToken;
    }

    /**
     * Set the reset token
     * @param resetToken The new reset token
     */
    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }
}
