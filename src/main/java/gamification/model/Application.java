package gamification.model;

import gamification.worker.KeyGenerator;

/**
 * @Class       : Application
 * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
 * @Date        : 14.11.2018
 *
 * @Goal        : Application model
 *
 * @Comment(s)  : -
 */
public class Application {

    private int id;
    private String name;
    private String description;
    private String apiKey;
    private String secretKey;
    private boolean enabled;
    private final static int KEY_LENGTH = 128;

    private Member member;

    public Application(int id, String name, String description, String apiKey, String secretKey, Member owner, boolean enabled) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.apiKey = apiKey;
        this.secretKey = secretKey;
        this.enabled = enabled;
        this.member = owner;
    }

    public Application(String name, String description, Member owner){
        this.name = name;
        this.description = description;
        this.member = owner;
        this.apiKey = KeyGenerator.getInstance().generateKey(KEY_LENGTH);
        this.secretKey = KeyGenerator.getInstance().generateKey(KEY_LENGTH);
    }

    /**
     * Get the application's name
     * @return The application's name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the application's name
     * @param name The application's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the application's description
     * @return The application's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the application's description
     * @param description The application's description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Define if the application's is enabled
     * @return true -> application, false -> disabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Set the application's account state
     * @param enabled The new application's state
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Get the application's id
     * @return The application's id
     */
    public int getId() {
        return id;
    }

    /**
     * Get the application's api key
     * @return The application's api key
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * Get the application's secret key
     * @return The application's secret key
     */
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * Get the application's owner
     * @return The application's owner
     */
    public Member getMember() {
        return member;
    }

    /**
     * Set the application's owner
     * @param member The application's owner
     */
    public void setMember(Member member) {
        this.member = member;
    }
}
