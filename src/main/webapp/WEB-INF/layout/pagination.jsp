<!--
    /**
    * @File        : pagination.jsp
    * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
    * @Date        : 14.11.2018
    *
    * @Goal        : pagination
    *
    * @Comment(s)  : -
    */
-->

    <nav>
        <ul class="pagination justify-content-end">
            <%
                int numberOfPages = (Integer) request.getAttribute("numberOfPages");
                int nbElementsPerPage = (Integer) request.getAttribute("nbElementsPerPage");
                int pageIndex = (Integer) request.getAttribute("pageIndex");
                String url = request.getParameter("url");
                if (!url.contains("?")) {
                    url += "?1=1";
                }
                if(pageIndex > 0)
                {
                    if(pageIndex > 1) {
                        out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"" + url + "&pageIndex=" + 0 + "&nbElementPerPage=" + nbElementsPerPage + "\">" + (1) + "</a></li>");
                        if(pageIndex > 2) {
                            out.println("<li class=\"page-item disabled\"><a class=\"page-link\" tabindex=\"-1\">...</a></li>");
                        }
                    }
                    out.println("<li class=\"page-item\"><a  class=\"page-link\" href=\"" + url + "&pageIndex=" + (pageIndex - 1) + "&nbElementPerPage=" + nbElementsPerPage + "\">" + (pageIndex) + "</a></li>");
                }
                out.println("<li class=\"page-item active\"><a class=\"page-link\" href=\"" + url + "&pageIndex=" + pageIndex + "&nbElementPerPage="+ nbElementsPerPage + "\">" + (pageIndex + 1) + "</a></li>");
                if(pageIndex < numberOfPages - 1)
                {
                    out.println("<li class=\"page-item\"><a id=\"next\" class=\"page-link\" href=\"" + url + "&pageIndex=" + (pageIndex + 1) + "&nbElementPerPage=" + nbElementsPerPage + "\">" + (pageIndex + 2) + "</a></li>");
                    if(pageIndex + 2 < numberOfPages) {
                        if((numberOfPages - 1) - (pageIndex + 2) > 0) {
                            out.println("<li class=\"page-item disabled\"><a class=\"page-link\" tabindex=\"-1\">...</a></li>");
                        }
                        out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"" + url + "&pageIndex=" + (numberOfPages - 1) + "&nbElementPerPage=" + nbElementsPerPage + "\">" + (numberOfPages) + "</a></li>");
                    }
                }
                %>
        </ul>
        </nav>
