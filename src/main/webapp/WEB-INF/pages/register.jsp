<!--
/**
* @File        : register.jsp
* @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
* @Date        : 14.11.2018
*
* @Goal        : register form
*
* @Comment(s)  : -
*/
-->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>Register</title>
    <jsp:include page="../layout/head.jsp"/>
  </head>
  <body class="public">
    <div class="container-fluid">
      <div class="row align-items-center justify-content-center content-wrapper">
        <div class="col-sm-3 pt-3 pb-3 content shadow bg-white rounded">
          <h1>Register</h1>
          <c:if test="${not empty errorMessage}">
            <div class="alert alert-danger" role="alert">
              <c:out value="${errorMessage}"/>
            </div>
          </c:if>
          <form method="post" action="register">
            <div class="form-group">
              <label for="login">Email:</label>
              <input type="email" class="form-control" id="login" name="login" aria-describedby="login" placeholder="Login">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" id="name" name="name" aria-describedby="name" placeholder="Name">
            </div>
            <div class="form-group">
              <label for="password">Password:</label>
              <input type="password" class="form-control" id="password" name="password" aria-describedby="password" placeholder="Password">
            </div>
            <div class="form-group">
              <label for="repeatPassword">Repeat:</label>
              <input type="password" class="form-control" id="repeatPassword" name="repeat" aria-describedby="repeatPassword" placeholder="Repeat password">
            </div>
            <a class="float-left mt-2" href="login">Already have an account ? Login.</a>
            <input type="submit" value="Register" id="submitButton" class="btn btn-primary float-right">
            <div class="clearfix"></div>
          </form>
        </div>
      </div>
    </div>
    <jsp:include page="../layout/footer.jsp"/>
  </body>
</html>
