<!--
    /**
    * @File        : applist.jsp
    * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
    * @Date        : 14.11.2018
    *
    * @Goal        : manage api key page
    *
    * @Comment(s)  : -
    */
-->
<%@ page import="java.util.*" contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="gamification.model.Application" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>applist</title>
    <jsp:include page="../../layout/head.jsp"/>
</head>
<body class="public">
    <div class="container">
        <div class="row align-items-center mt-3 mb-3">
            <div class="col-sm-6">
                <h1 class="text-white">Welcome ${user.name}</h1>
            </div>
            <div class="col-sm-6 text-right">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ${user.email}
                    </button>
                    <form method="post" action="logout" class="m-0">
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <span class="dropdown-item-text"><b>Role:</b> ${user.role}</span>
                            <button class="dropdown-item text-danger" type="submit" id="logout">Logout</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container bg-white shadow">
        <div class="row">
            <div class="col-sm-12 p-4">
                <c:if test="${user.role=='ADMINISTRATOR'}">
                    <a href="admin" class="btn btn-warning float-right">Admin</a>
                </c:if>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-transparent p-0">
                        <li class="breadcrumb-item active" aria-current="page">Home</li>
                    </ol>
                </nav>
                <h1 class="mb-3">My applications</h1>
                <div class="row">
                    <div class="col-sm-2"><label for="name">Email : </label></div>
                    <div class="col-sm-6 font-weight-bold">${user.email}</div>
                </div>
                <div class="row mt-2 mb-3">
                    <div class="col-sm-2"><label for="name">Name : </label></div>
                    <div class="col-sm-6 font-weight-bold">${user.name}</div>
                </div>
                <a href="application" class="btn btn-primary">Add application</a>

                <jsp:include page="app_list_partial.jsp"/>
                <jsp:include page="../../layout/pagination.jsp">
                    <jsp:param name="url" value="${baseUrl}/applist" />
                </jsp:include>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-white text-center mt-3">
                Copyright HEIG
            </div>
        </div>
    </div>

<jsp:include page="../../layout/footer.jsp"/>
</body>
</html>
