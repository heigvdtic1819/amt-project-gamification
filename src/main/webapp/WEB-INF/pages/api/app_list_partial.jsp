<%@ page import="gamification.model.Application" %>
<%@ page import="java.util.List" %>
<div class="table-responsive">
    <table class="table mt-5">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">API Key</th>
            <th scope="col">State</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Application> applications = (List) request.getAttribute("applications");
            String baseUrl = (String)request.getAttribute("baseUrl");
            for(Application app : applications) {
                String checked = app.isEnabled() ? "checked" : "";
                out.println("<tr><td>"+ app.getId() +"</td><td>" + app.getName() + "</td><td>" + app.getDescription() + "</td><td>" + app.getApiKey() + "</td><td><input type=\"checkbox\" "+ checked + " data-toggle=\"toggle\" disabled class=\"disabled\" /></td><td><a href=\""+ baseUrl +"/appdetail?id="+ app.getId() +"\" class=\"btn btn-primary\">Details</a></td></tr>");
            }
        %>

        </tbody>
    </table>
</div>