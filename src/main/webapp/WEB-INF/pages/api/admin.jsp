<!--
    /**
    * @File        : admin.jsp
    * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
    * @Date        : 14.11.2018
    *
    * @Goal        : admin management console
    *
    * @Comment(s)  : -
    */
-->

<%@ page import="java.util.*" contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="gamification.model.Member" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Admin</title>
    <jsp:include page="../../layout/head.jsp"/>
</head>
<body class="public">
    <div class="container">
        <div class="row align-items-center mt-3 mb-3">
            <div class="col-sm-6">
                <h1 class="text-white">Welcome ${user.name} </h1>
            </div>
            <div class="col-sm-6 text-right">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ${user.email}
                    </button>
                    <form method="post" action="${baseUrl}/logout" class="m-0">
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <span class="dropdown-item-text"><b>Role:</b> ${user.role}</span>
                            <button class="dropdown-item text-danger" type="submit" id="logout">Logout</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container bg-white shadow">
        <div class="row">
            <div class="col-sm-12 p-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-transparent p-0">
                        <li class="breadcrumb-item"><a href="applist">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Admin</li>
                    </ol>
                </nav>
                <h1>Users administration</h1>
                <div class="table-responsive">
                    <table class="table mt-5">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Email</th>
                            <th scope="col">Name</th>
                            <th scope="col">Role</th>
                            <th scope="col">Enabled</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            List<Member> members = (List) request.getAttribute("members");
                            for(int i = 0; i < members.size(); i++){
                                out.println("<tr><td></td><td>" + members.get(i).getEmail() + "</td><td>" + members.get(i).getName() + "</td><td>"+ members.get(i).getRole() +"</td><td>" + members.get(i).isEnabled() + "</td><td><a href='admin/member?email=" + members.get(i).getEmail() + "' class='btn btn-primary'>détails</a></td></tr>");
                            }
                        %>
                        </tbody>
                    </table>
                </div>
                <jsp:include page="../../layout/pagination.jsp">
                    <jsp:param name="url" value="${baseUrl}/admin" />
                </jsp:include>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-white text-center mt-3">
                Copyright HEIG
            </div>
        </div>
    </div>

<jsp:include page="../../layout/footer.jsp"/>
</body>
</html>