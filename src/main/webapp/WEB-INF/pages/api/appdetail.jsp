<!--
/**
* @File        : member.jsp
* @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Moska & Lenny Aebischer
* @Date        : 14.11.2018
*
* @Goal        : manage member page
*
* @Comment(s)  : -
*/
-->
<%@ page import="java.util.*" contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="gamification.model.Member" %>
<%@ page import="gamification.model.Application" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Admin</title>
    <jsp:include page="../../layout/head.jsp"/>
</head>
<body class="public">
<div class="container">
    <div class="row align-items-center mt-3 mb-3">
        <div class="col-sm-6">
            <h1 class="text-white">Welcome ${user.name}</h1>
        </div>
        <div class="col-sm-6 text-right">
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ${user.email}
                </button>
                <form method="post" action="${baseUrl}/logout" class="m-0">
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <span class="dropdown-item-text"><b>Role:</b> ${user.role}</span>
                        <button class="dropdown-item text-danger" type="submit">Logout</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container bg-white shadow">
    <div class="row">
        <div class="col-sm-12 p-4">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent p-0">
                    <li class="breadcrumb-item"><a href="${baseUrl}/applist">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">${app.name}</li>
                </ol>
            </nav>
            <h1>Application detail</h1>
            <div class="row">
                <div class="col-sm-12">
                    <c:if test="${not empty errorMessage}">
                        <div class="alert alert-danger" role="alert">
                            <c:out value="${errorMessage}"/>
                        </div>
                    </c:if>
                    <c:if test="${not empty successMessage}">
                        <div class="alert alert-success" role="alert">
                            <c:out value="${successMessage}"/>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-right">
                    <a href="appdetail?id=${app.id}" id="delete-app" class="btn btn-danger">Delete app</a>
                </div>
            </div>
            <form method="post">
                <div class="row mt-2">
                    <div class="col-sm-2"><label for="name">Name : </label></div>
                    <div class="col-sm-6 font-weight-bold"><input placeholder="Name" class="form-control" id="name" name="name" type="text" value="${app.name}"></div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-2"><label for="description">Description : </label></div>
                    <div class="col-sm-6 font-weight-bold">
                       <textarea class="form-control" id="description" name="description" rows="3">${app.description}</textarea>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-2"><label for="enabled">Enabled : </label></div>
                    <div class="col-sm-6 font-weight-bold">
                        <%
                            boolean checked = ((Application)request.getAttribute("app")).isEnabled();
                        %>
                        <input type="checkbox" id="enabled" name="enabled" <% if (checked) { %> checked <% } %> data-toggle="toggle" />
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
            <div class="row mt-2">
                <div class="col-sm-2"><label for="enabled">API Key : </label></div>
                <div class="col-sm-6 font-weight-bold">
                    ${app.apiKey}
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-2"><label for="enabled">API Secret : </label></div>
                <div class="col-sm-6 font-weight-bold">
                    ${app.secretKey}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12 text-white text-center mt-3">
            Copyright HEIG
        </div>
    </div>
</div>

<jsp:include page="../../layout/footer.jsp"/>
<script>
    $('#delete-app').click(function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        console.log(url);
        $.ajax({
            url,
            method: 'DELETE',
            success: function(data) {
                window.location = '${baseUrl}/applist';
            }
        });
    });
</script>
</body>
</html>