<!--
    /**
    * @File        : member.jsp
    * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
    * @Date        : 14.11.2018
    *
    * @Goal        : manage member page
    *
    * @Comment(s)  : -
    */
-->
<%@ page import="java.util.*" contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="gamification.model.Member" %>
<%@ page import="gamification.model.Application" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Admin</title>
    <jsp:include page="../../layout/head.jsp"/>
</head>
<body class="public">
<div class="container">
    <div class="row align-items-center mt-3 mb-3">
        <div class="col-sm-6">
            <h1 class="text-white">Welcome ${user.name}</h1>
        </div>
        <div class="col-sm-6 text-right">
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ${user.email}
                </button>
                <form method="post" action="${baseUrl}/logout" class="m-0">
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <span class="dropdown-item-text"><b>Role:</b> ${user.role}</span>
                        <button class="dropdown-item text-danger" type="submit">Logout</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container bg-white shadow">
    <div class="row">
        <div class="col-sm-12 p-4">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent p-0">
                    <li class="breadcrumb-item"><a href="${baseUrl}/applist">Home</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="${baseUrl}/admin">Admin</a></li>
                    <li class="breadcrumb-item active" aria-current="page">${member.email}</li>
                </ol>
            </nav>
            <h1>Users administration</h1>
            <div class="row">
                <div class="col-sm-12">
                    <c:if test="${not empty errorMessage}">
                        <div class="alert alert-danger" role="alert">
                            <c:out value="${errorMessage}"/>
                        </div>
                    </c:if>
                    <c:if test="${not empty successMessage}">
                        <div class="alert alert-success" role="alert">
                            <c:out value="${successMessage}"/>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-right">
                    <form method="post" action="${baseUrl}/admin/reset?email=${member.email}">
                        <button type="submit" class="btn btn-warning">Reset password</button>
                        <a href="member?email=${member.email}" id="delete-user" class="btn btn-danger">Delete user</a>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">Email : </div><div class="col-sm-10 font-weight-bold">${member.email}</div>
            </div>
            <form method="post">
                <div class="row mt-2">
                    <div class="col-sm-2"><label for="name">Name : </label></div>
                    <div class="col-sm-6 font-weight-bold"><input placeholder="Name" class="form-control" id="name" name="name" type="text" value="${member.name}"></div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-2"><label for="role">Role : </label></div>
                    <div class="col-sm-6 font-weight-bold">
                        <%
                            int currentRole = ((Member)request.getAttribute("member")).getRole().ordinal();
                        %>
                        <select name="role" id="role" class="form-control">
                            <option value="0" <% if (currentRole == 0) { %>selected<%}%>>Admin.</option>
                            <option value="1" <% if (currentRole == 1) { %>selected<%}%>>Developer</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-2"><label for="enabled">Enabled : </label></div>
                    <div class="col-sm-6 font-weight-bold">
                        <%
                            boolean checked = ((Member)request.getAttribute("member")).isEnabled();
                        %>
                        <input type="checkbox" id="enabled" name="enabled" <% if (checked) { %> checked <% } %> data-toggle="toggle" />
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>

            <jsp:include page="app_list_partial.jsp"/>
            <jsp:include page="../../layout/pagination.jsp">
                <jsp:param name="url" value="${baseUrl}/admin/member?email=${member.email}" />
            </jsp:include>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12 text-white text-center mt-3">
            Copyright HEIG
        </div>
    </div>
</div>

<jsp:include page="../../layout/footer.jsp"/>
<script>
    $('#delete-user').click(function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        console.log(url);
        $.ajax({
            url,
           method: 'DELETE',
            success: function(data) {
                window.location = '${baseUrl}/admin';
            }
        });
    });
</script>
</body>
</html>