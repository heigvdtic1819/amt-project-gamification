<!--
    /**
    * @File        : application.jsp
    * @Author(s)   : Michael Brouchoud, Thomas Lechaire, Alexandre Mosca & Lenny Aebischer
    * @Date        : 14.11.2018
    *
    * @Goal        : create application form
    *
    * @Comment(s)  : -
    */
-->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Application</title>
    <jsp:include page="../../layout/head.jsp"/>
</head>
<body class="public">
    <div class="container">
        <div class="row align-items-center mt-3 mb-3">
            <div class="col-sm-6">
                <h1 class="text-white">Welcome ${user.name}</h1>
            </div>
            <div class="col-sm-6 text-right">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ${user.email}
                    </button>
                    <form method="post" action="logout" class="m-0">
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <span class="dropdown-item-text"><b>Role:</b> ${user.role}</span>
                            <button class="dropdown-item text-danger" type="submit">Logout</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container bg-white shadow">
        <div class="row">
            <div class="col-sm-12 p-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-transparent p-0">
                        <li class="breadcrumb-item"><a href="applist">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add</li>
                    </ol>
                </nav>
                <h1>Add application</h1>
                <c:if test="${not empty resultat}">
                <div class="alert alert-danger">
                    ${resultat}
                </div>
                </c:if>
                <form method="post" action="application">

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="nom" placeholder="Name of application" value="<c:out value="${param.nom}"/>">
                        <small class="text-danger form-text">${erreurs['nom']}</small>
                    </div>




                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description" rows="3"><c:out value="${param.description}"/></textarea>
                        <small class="text-danger form-text">${erreurs['description']}</small>
                    </div>

                     <input type="submit" id="save" value="Save" class="btn btn-primary float-right" />



                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-white text-center mt-3">
                Copyright HEIG
            </div>
        </div>
    </div>

<jsp:include page="../../layout/footer.jsp"/>

</body>
</html>
