CREATE TABLE members(
	email VARCHAR(45) NOT NULL,
	name VARCHAR(45) NOT NULL,
	password VARCHAR(45) NOT NULL,
	enabled TINYINT(1) NOT NULL,
	password_resetted TINYINT(1) NOT NULL,
	reset_token VARCHAR(255),
	role INT NOT NULL,
	PRIMARY KEY (email),
	UNIQUE (email)
);

CREATE TABLE applications(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(45),
	description VARCHAR(45),
	api_key VARCHAR(256),
	api_secret VARCHAR(256),
	enabled TINYINT(1) NOT NULL DEFAULT 1,
	fk_members VARCHAR(45),
	FOREIGN KEY (fk_members) REFERENCES members(email) ON DELETE CASCADE,
	PRIMARY KEY (id),
	UNIQUE (id)
);
