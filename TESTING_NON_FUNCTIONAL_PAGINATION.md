# JMeter tests
## Context
The JMeter tests execute 300 Threads which each of them retrieves a user with 1000 applications.
The idea is to compare the request time between a request without pagination and with pagination.
The second part is to compare the request time if we set the payara pool request size.

## Results

### Max pool 50

![1000App-vs-5AppPagination-max-pool-50](./img/tests/jmeter/01-1000App-vs-5AppPagination-max-pool-50.png)

### Max pool 100

![1000App-vs-5AppPagination-max-pool-100.](./img/tests/jmeter/02-1000App-vs-5AppPagination-max-pool-100.png)

### Max pool 150

![1000App-vs-5AppPagination-max-pool-150](./img/tests/jmeter/03-1000App-vs-5AppPagination-max-pool-150.png)

### Max pool 200

![1000App-vs-5AppPagination-max-pool-200.](./img/tests/jmeter/04-1000App-vs-5AppPagination-max-pool-200.png)

## Headers meaning

- **Libellé :** id of the test
- **Echantillons :** Number of executed requests
- **Moyenne (ms) :** Average of respond time far all requests
- **Min (ms) :** Minimal time request 
- **Max (ms) :** Maximal time request
- **Ecart type (ms) :** Distribution of the "echantillions" near the average
- **% Erreur :** Pourcent of error requests
- **Débit :**  Request debit
- **Ko/sec :** Speed time transfer
- **Moy. octets :** Average of octet transfered

## Interpretation of results

It was really hard to extract something from all thoses results as the informations weren't clear enough. We did the tests 
although it was not clear. We saw that the app was responding quite good to the amount of thread we tests. Maybe a clearer
explaination on the goal and needs of thoses tests could lead us to a deeper understanding. 

What we saw is that the app is responding quite good to all charge tests.