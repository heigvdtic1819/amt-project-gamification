# AMT - Project Gamification

## Setup environment

Download [payara server](http://payara.fish/). Unzip the folder where you want (Recommended on Windows : C root folder).

Download [mysql-jdbc-connector](https://dev.mysql.com/downloads/connector/j/) and place the mysql-connector-java-x.x.xx.jar file in the folder `./payara5/glassfish/lib`

Open a terminal and go to the folder `./payara5/glassfish/bin` and run the followed command

```shell
asadmin
start-domain
```

Connect to the admin panel with a browser [http://localhost:4848](http:localhost:4848)

### Dev environment

On the left menu go to Resources => JavaMail Sessions and click on "New" and add this configurations

- `JNDI Name : mail/MailSession`
- `Mail Host : gmail.com`
- `Default User : lamt.gamification@gmail.com`
- `Password : HEIG$2018`
- `Default Sender Address : lamt.gamification@gmail.com`

Configure the Additional Properties :
- `mail.smtp.auth : true`
- `mail.smtp.host : smtp.gmail.com`
- `mail.smtp.port : 587`
- `mail.smtp.starttls.enable : true`

Then go to Resource => JDBC => JDBC Connection Pools and click on "New" and add this configurations :

- `Pool Name : mysql-amt-gramification-pool`
- `Resource Type : javax.sql.DataSource`
- `Database Driver Vendor : MySql`

Click on Next

Configure the Datasource ClassName like this : `com.mysql.cj.jdbc.MysqlDataSource`

Configure the Additional Properties :

- `user : heig`
- `password : HEIG$2018`
- `url : jdbc:mysql://pan.kreativmedia.ch:3306/amt_heig`
- `useSSL : false`
- `serverTimezone : UTC`

Click on "Finish"

You can test the pool connection if you click on "mysql-amt-gramification-pool" and then on "Ping"

Finally Go to Resource => JDBC => JDBC Resources and click on "New" and add this configurations :

- `JNDI Name : jdbc/AMTDatasource`
- `Pool Name : mysql-amt-gramification-pool`

### Test no functional environment

Go to Resource => JDBC => JDBC Connection Pools and click on "New" and add this configurations :

- `Pool Name : mysql-amt-gramification-test-pool`
- `Resource Type : javax.sql.DataSource`
- `Database Driver Vendor : MySql`

Click on Next

Configure the Datasource ClassName like this : `com.mysql.cj.jdbc.MysqlDataSource`

Configure the Additional Properties :

- `user : heig`
- `password : HEIG$2018`
- `url : jdbc:mysql://localhost:3306/amt_heig`
- `useSSL : false`
- `serverTimezone : UTC`

Click on "Finish"

You can test the pool connection if you click on "mysql-amt-gramification-test-pool" and then on "Ping" (You must have mount the docker test container if you want the ping works, follow the "Setup for testings" chapter to know how to do that)

Open the command before used to use asadmin and use the following command

```shell
stop-domain
exit
```

## IHM Testing with Selenium
You can find these test on this link [https://github.com/thomaslc66/amt-project-gamification-tests](https://github.com/thomaslc66/amt-project-gamification-tests)

## JMeter Testing
Download [JMeter](https://jmeter.apache.org/download_jmeter.cgi)
Download [jmeter-plugins.org](https://jmeter-plugins.org/install/Install/)
Launch JMeter and go to Option => Plugin Manager => Available Plugins and install Custom Threads Groups

Import the JMeter config files `./jmeter/get_applist_jmeter.jmx`

The results are in `./TESTING_NON_FUNCTIONAL_PAGINATION.md` file

## Setup for no functional testings

To run tests, you must before run docker container which are in `./dev_images` folder. You can run them with the followed command on the `./dev_image folder`

The results are in `./TESTING_NON_FUNCTIONAL_TRANSACTIONS.md` file

```shell
docker-compose up --build -d
```

To use the container as localhost on Windows and Mac with VirtualBox you can follow this [Guide](https://www.jhipster.tech/tips/020_tip_using_docker_containers_as_localhost_on_mac_and_windows.html)

You can access to the phpmyadmin with this address [http://localhost:6060](http:localhost:6060)

- login : heig
- password : HEIG$2018

You must maybe wait and try few times to login

Connect to phpmyadmin and use the commands in file `./sql/create_database.sql` to create the tables

Launch Payara server and go to Resource => JDBC => JDBC Resources and click on AMTDatasource
Swap the pool to `mysql-amt-gramification-test-pool`

## Deployment
To deploy the app with a docker machine, go with a terminal to the `./prod_ready folder` and run the following command
```shell
docker-compose up --build -d
```

## Improvement