# No Functional Tests
## Test structure
To do these experimental tests, a special directory has been added on `./src/main/java/gamification/tests` folder

### ApplicationNoFunctionTestServlet

![ApplicationNoFunctionTestServlet](./img/tests/no-functional/01-ApplicationNoFunctionalTestServlet.png)

A special servel has been implemented, it is use to display the results of the insertion to the browser.

Each time the user refresh the page [http://localhost:8080/ch.heigvd.amt-1.0/tests](http://localhost:8080/ch.heigvd.amt-1.0/tests), this will create a new member with 4 applications.

The idea is to display if there was an error when the application tried to create a new member with 4 applications and display the number of applications created before and after the transaction.

When the Servlet is initialized the applications create an exception member, this member is required when the member transaction throw an exception for testing because we need one to create applications. So this is useful to test different transaction type scenarios.


### ApplicationNoFunctionalTestDAO

![ApplicationNoFunctionalTestDAO-a](./img/tests/no-functional/02-ApplicationNoFunctionalTestDAO-a.png)
![ApplicationNoFunctionalTestDAO-b](./img/tests/no-functional/02-ApplicationNoFunctionalTestDAO-b.png)

The idea here is to create a new member and add 4 new applications for this member.

The code  and the `TransactionAttribute` commented is here to show the good way to use call a method inside the same EJB.

### ApplicationNoFunctionalTestDAOLocal

![ApplicationNoFunctionalTestDAOLocal](./img/tests/no-functional/03-ApplicationNoFunctionalTestDAOLocal.png)

A standard DAO Local to implement


### ApplicationNoFunctionalTestMemberInfo

![ApplicationNoFunctionalTestMemberInfo](./img/tests/no-functional/04-ApplicationNoFunctionalTestMemberInfo.png)

This can is only use for testing. The idea is to have the member and the applications inside one class so it is easier to display informations to user.


### MemberJdbcDAO

![MemberJdbcDAO](./img/tests/no-functional/05-MemberJdbcDAO.png)

We add special parameter to throw exception for testing

### ApplicationJdbcDAO

![ApplicationJdbcDAO](./img/tests/no-functional/05-ApplicationJdbcDAO.png)

There is `TransactionAttribute`  to tests different transaction scenario

### TransactionAttributeType

![TransactionAttributeType-a](./img/tests/no-functional/06-TransactionAttributeType-a.png)

We can see in the documentation that the `TransactionAttribute` can have different `TransactionAttributeType`.

![TransactionAttributeType-b](./img/tests/no-functional/06-TransactionAttributeType-b.png)

As we can see here, the `TransactionAttributeType` can only set to these types for an Stateless/Statefull EJB
- REQUIRED
- REQUIRES_NEW
- NOT_SUPPORTED

The default `TransactionAttributeType` is `REQUIRED`
We will not test NOT_SUPPORTED because this type is use when the transaction is not supported and it is not the case here

## Required tests
### No error during transaction
For these tests, the `TransactionAttributeType` in `ApplicationJdbcDAO` and `ApplicationNoFunctionalTestDAO` class are set to `REQUIRED` to be sure that the works as expected.

If no error is throw the applications add 1 member and 4 applications every time the user refresh the browser

![Required-test-a](./img/tests/no-functional/07-Required-test-a.png)

![Required-test-b](./img/tests/no-functional/07-Required-test-b.png)

If we check the database we can see that there are 2 members and 8 applications created

(The Exception member is there only if there is an exception thrown)

![Required-test-c](./img/tests/no-functional/07-Required-test-c.png)

![Required-test-d](./img/tests/no-functional/07-Required-test-d.png)

### Error during transaction
For this test the `MemberJdbcDAO` will throw an exception.

We can see here that there are no member and no applications created every time the user refresh the browser

![Required-test-error-a](./img/tests/no-functional/08-Required-test-error-a.png)

If we check the database we can see that there are no member and no applications created

(The Exception member is there only if there is an exception thrown)

![Required-test-error-b](./img/tests/no-functional/08-Required-test-error-b.png)

![Required-test-error-c](./img/tests/no-functional/08-Required-test-error-c.png)

The conclusion is that with these configurations if a transaction failed all transaction are cancelled and rollback as expected.

## Requires new test
### No error during transaction
For these tests, the `TransactionAttributeType` in `ApplicationJdbcDAO` and `ApplicationNoFunctionalTestDAO` class are set to `REQUIRES_NEW`.

If no error is throw the applications add 1 member and 4 applications every time the user refresh the browser

![Requires-New-test-a](./img/tests/no-functional/09-Requires-New-test-a.png)

![Requires-New-test-b](./img/tests/no-functional/09-Requires-New-test-b.png)

If we check the database we can see that there are 2 members and 8 applications created

(The Exception member is there only if there is an exception thrown)

![Requires-New-test-c](./img/tests/no-functional/09-Requires-New-test-c.png)


![Requires-New-test-d](./img/tests/no-functional/09-Requires-New-test-d.png)

### Error during transaction
For this test the `MemberJdbcDAO` will throw an exception.

We can see that an error is display to the user but the applications are created

![Requires-New-test-error-a](./img/tests/no-functional/10-Requires-New-test-error-a.png)

![Requires-New-test-error-b](./img/tests/no-functional/10-Requires-New-test-error-b.png)

If we check the database we can see that there are no member but the applications has been created

(The Exception member is there only if there is an exception thrown)

![Requires-New-test-error-c](./img/tests/no-functional/10-Requires-New-test-error-c.png)

![Requires-New-test-error-d](./img/tests/no-functional/10-Requires-New-test-error-d.png)

We can see here that the transactions works as expected and continue to create applications if the member transaction 
failed because there are execute in an another transaction.

## Call this issue
For these tests we will use `this.addMember()` call inside the method `ApplicationNoFunctionalTestDAO.addApplicationNoFunctionalTestMemberInfo()` method
to see what can issue could happened with different `TransactionAttributeType`

For these tests, the `TransactionAttributeType` in `ApplicationJdbcDAO` and `ApplicationNoFunctionalTestDAO` class are set to `REQUIRES_NEW`
because the issue can only happen with `REQUIRES_NEW`.

If you test with this configuration there is an issue with because we use a call to a method inside the same EJB
and the `ApplicationNoFunctionalTestDAO` do not start a new transaction.

![This-Issue-test-a](./img/tests/no-functional/11-This-Issue-test-a.png)

If we check the server logs we can see that there is a timeout error in the transaction

![This-Issue-test-b](./img/tests/no-functional/11-This-Issue-test-b.png)

After a long time to wait, the application display this to the user.

We can see that the user was created but not the applications.

## Conclusion
We can see that we can use `TransactionAttributeType` to execute new transaction. It can be useful if we want to separate
the transaction and be sure to save all the successful request.

Furthermore, we can see that if we want to call a method inside an EJB can cause many issues
and it is a bad practice. The best way is to delegate the method to a new EJB to prevent this kind of issues.



